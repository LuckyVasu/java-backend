package com.onlinecompiler.service;

import java.io.File;

public interface PythonCompilerService {

	public File createFile();

	public String execute(String content);
}
