package com.onlinecompiler.service;

import java.io.File;

public interface CppCompilerService {

	public File createFile();

	public String execute(String content);
}
