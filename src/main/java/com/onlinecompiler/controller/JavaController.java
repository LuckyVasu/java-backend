package com.onlinecompiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinecompiler.service.JavaCompilerService;

@RestController
@CrossOrigin
public class JavaController {
	
	//@Autowired and we need to create object for JavaCompilerService to execute its method execute
	@Autowired
	private JavaCompilerService javaCompilerService;
// this one will execute too but it will get json so going for below method to get editor value as String
//	@PostMapping("/test") 
//	public String test(@RequestBody String requestBody) {
//		System.out.println("called" + requestBody);
//		return "SUCCESS";
//	}
	@PostMapping("/compileContent")
	public String test(@RequestParam String content) {
		System.out.println("called" + content);
		return javaCompilerService.execute(content);
	}
//http://localhost:8080/compileContent?content=hiii  this will exectute from chromeAPI, content is the key we use in angular java-compiler ts file
}
