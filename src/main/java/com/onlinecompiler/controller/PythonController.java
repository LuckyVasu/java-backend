package com.onlinecompiler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlinecompiler.service.PythonCompilerService;

@RestController
@CrossOrigin
public class PythonController {
	@Autowired
	private PythonCompilerService pythonCompilerService;
	@PostMapping("/compilePython")
	public String test(@RequestParam String content) {
		System.out.println("called" + content);
		return pythonCompilerService.execute(content);
		//return "success";
	}
}
