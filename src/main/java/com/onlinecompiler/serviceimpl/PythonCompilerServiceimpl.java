package com.onlinecompiler.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.onlinecompiler.service.PythonCompilerService;

@Service
public class PythonCompilerServiceimpl implements PythonCompilerService {

	@Override
	public File createFile() {
		// TODO Auto-generated method stub
		String dir = System.getProperty("user.dir");

		File pythoncompiler = new File(dir + "\\pythoncompiler");
		if (pythoncompiler.exists()) {
			System.out.println(pythoncompiler + " exists");

		} else {
			pythoncompiler.mkdir();
			System.out.println(pythoncompiler + "  not exists so created");
		}
		String filename = "editor.py";
		File file = new File(pythoncompiler + "/" + filename);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}

	@Override
	public String execute(String content) {
		RandomAccessFile raf;
		String dir = System.getProperty("user.dir");
		File pythoncompiler = new File(dir + "\\pythoncompiler");
		if (pythoncompiler.exists()) {
			System.out.println(pythoncompiler + " exists");
		} else {
			pythoncompiler.mkdir();
			System.out.println(pythoncompiler + "  not exists so created");
		}

		String filename = "editor.py";
		File file = new File(pythoncompiler + "/" + filename);
		try {
			raf = new RandomAccessFile(file, "rw");
			raf.writeBytes(content);
			raf.close();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String errorContent = "program failed";
		try {
			removeLogFile(pythoncompiler.getAbsolutePath() + "/" + "output.log");
			//ProcessBuilder is a class which executes file
			ProcessBuilder builder = new ProcessBuilder("python3", file.getAbsolutePath());
			builder.redirectErrorStream(true);
			builder.redirectOutput(new File(pythoncompiler.getAbsolutePath() + "/" + "output.log"));
			Process process = builder.start();
			Thread.sleep(2000);

			String outputContent = new String(
					Files.readAllBytes(Paths.get(pythoncompiler.getAbsolutePath() + "/" + "output.log")));
			System.out.println(outputContent);
			return outputContent;
		} catch (IOException | InterruptedException e1) {
			e1.printStackTrace();
			errorContent = e1.getMessage();
		}

		return errorContent;
	}

	private void removeLogFile(String fileName) {
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
			System.out.println( fileName + "  deleted");
		}
	}
}
